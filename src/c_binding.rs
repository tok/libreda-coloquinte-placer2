// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! C bindings for coloquinte.

use crate::CellOrientation;
use log;

/// Bundle of parameters used by Coloquinte.
///
/// Use [`ColoquinteParameters::check()`] to verify consistency of the parameter values.
#[derive(Clone, Debug)]
pub(crate) struct ColoquinteParameters {
    /// Number of cell instances in the circuit.
    pub num_cells: usize,
    /// Number of nets.
    pub num_nets: usize,
    /// Size of the cells (widths). Must contain `num_cells` elements.
    pub cell_widths: Vec<i32>,
    /// Size of the cells (heights). Must contain `num_cells` elements.
    pub cell_heights: Vec<i32>,
    /// Tell if a cell is fixed (`true`) and cannot be moved by the placer.
    /// Must contain `num_cells` elements.
    pub cell_fixed: Vec<bool>,
    pub net_limits: Vec<i32>,
    pub pin_cells: Vec<i32>,
    /// Offset in x-direction of pins relative to the location of the cell instance.
    pub pin_x_offsets: Vec<i32>,
    /// Offset in y-direction of pins relative to the location of the cell instance.
    pub pin_y_offsets: Vec<i32>,
    /// x-position of the cells.
    pub cell_x: Vec<i32>,
    /// y-position of the cells.
    pub cell_y: Vec<i32>,
    /// Orientations of the cells.
    pub cell_orientation: Vec<i32>,
    /// Number of rows.
    pub num_rows: usize,
    /// Lower x-boundaries of the rectangles which describe the rows.
    pub row_min_x: Vec<i32>,
    /// Upper x-boundaries of the rectangles which describe the rows.
    pub row_max_x: Vec<i32>,
    /// Lower y-boundaries of the rectangles which describe the rows.
    pub row_min_y: Vec<i32>,
    /// Upper y-boundaries of the rectangles which describe the rows.
    pub row_max_y: Vec<i32>,
    /// Placement effort. Should be in the range [1..9].
    pub effort: i32,
}

impl Default for ColoquinteParameters {
    fn default() -> Self {
        Self {
            num_cells: Default::default(),
            num_nets: Default::default(),
            cell_widths: Default::default(),
            cell_heights: Default::default(),
            cell_fixed: Default::default(),
            net_limits: Default::default(),
            pin_cells: Default::default(),
            pin_x_offsets: Default::default(),
            pin_y_offsets: Default::default(),
            cell_x: Default::default(),
            cell_y: Default::default(),
            cell_orientation: Default::default(),
            num_rows: Default::default(),
            row_min_x: Default::default(),
            row_max_x: Default::default(),
            row_min_y: Default::default(),
            row_max_y: Default::default(),
            effort: 1,
        }
    }
}

impl ColoquinteParameters {
    /// Validate parameter values for consistency.
    /// Panics if something is not consistent.
    pub fn check(&self) {
        // Validate lengths of arrays.
        assert_eq!(self.cell_widths.len(), self.num_cells);
        assert_eq!(self.cell_heights.len(), self.num_cells);
        assert_eq!(self.cell_fixed.len(), self.num_cells);
        assert_eq!(self.net_limits.len(), self.num_nets + 1);
        assert!(self.net_limits.len() > 0);

        let num_pins = self.net_limits[self.net_limits.len() - 1]
            .try_into()
            .expect("can't convert to i32");

        // Pin definitions.
        assert_eq!(self.pin_cells.len(), num_pins);

        // Pin offsets relative to cells.
        assert_eq!(self.pin_x_offsets.len(), num_pins);
        assert_eq!(self.pin_y_offsets.len(), num_pins);

        // Cell positions.
        assert_eq!(self.cell_x.len(), self.num_cells);
        assert_eq!(self.cell_y.len(), self.num_cells);
        assert_eq!(self.cell_orientation.len(), self.num_cells);

        assert!(self
            .cell_orientation
            .iter()
            .all(|o| CellOrientation::try_from_int(*o).is_some()));

        // Row definitions.
        assert_eq!(self.row_min_x.len(), self.num_rows);
        assert_eq!(self.row_max_x.len(), self.num_rows);
        assert_eq!(self.row_min_y.len(), self.num_rows);
        assert_eq!(self.row_max_y.len(), self.num_rows);

        assert!(
            (0..self.num_rows).all(|i| self.row_min_x[i] <= self.row_max_x[i]),
            "row_min_x can't be larger than row_max_x"
        );

        assert!(
            (0..self.num_rows).all(|i| self.row_min_y[i] <= self.row_max_y[i]),
            "row_min_y can't be larger than row_max_y"
        );

        assert!(
            self.effort >= 1 && self.effort <= 9,
            "placement effort must be in the range [1..9]"
        );
    }
}

#[test]
fn test_coloquinte_3_cells() {
    // Place 3 cells. Two have an already fixed position.

    // Positions of fixed cells.
    let p0 = (100, 200);
    let p2 = (700, 800);

    let mut params = {
        let mut params = ColoquinteParameters::default();

        let row_height = 10;

        // Row definitions.
        {
            let num_rows = 100;
            let row_start = 0;
            let row_min_x = 0;
            let row_max_x = 1000;

            params.num_rows = num_rows;

            params.row_min_x = vec![row_min_x; num_rows];
            params.row_max_x = vec![row_max_x; num_rows];

            params.row_min_y = (0..num_rows as i32)
                .map(|i| row_start + i * row_height)
                .collect();

            params.row_max_y = (0..num_rows as i32)
                .map(|i| row_start + (i + 1) * row_height)
                .collect();
        }

        params.num_cells = 3;
        params.num_nets = 2;

        params.cell_widths = vec![10; params.num_cells];
        params.cell_heights = vec![row_height; params.num_cells];

        params.cell_fixed = vec![true, false, true];

        params.net_limits = vec![0, 2, 4]; // Has two nets, and four pins.
        params.pin_cells = vec![
            // net1
            0, 1, // Attached to cell 0 and 1
            // net2
            1, 2, // Attached to cell 1 and 2
        ];
        params.pin_x_offsets = vec![0, 0, 0, 0];
        params.pin_y_offsets = vec![0, 0, 0, 0];

        // Cell 0 is fixed at (100, 200)
        // Cell 1 is movable
        // Cell 2 is fixed at (700, 800)
        params.cell_x = vec![p0.0, 0, p2.0];
        params.cell_y = vec![p0.1, 0, p2.1];

        params.cell_orientation = vec![Default::default(); params.num_cells];

        params
    };

    params.check();

    let return_value = coloquinte_place(&mut params);

    dbg!(&params.cell_x);
    dbg!(&params.cell_y);

    assert_eq!(return_value, 0);

    {
        let msg = "fixed cells should still be at the initial location";
        assert_eq!(params.cell_x[0], p0.0, "{}", msg);
        assert_eq!(params.cell_y[0], p0.1, "{}", msg);
        assert_eq!(params.cell_x[2], p2.0, "{}", msg);
        assert_eq!(params.cell_y[2], p2.1, "{}", msg);
    }

    // Check position of movable cell.
    {
        // It should be somewhere in the middle of the two fixed cells.
        let (x, y) = (params.cell_x[1], params.cell_y[1]);

        assert!(x >= p0.0 && x <= p2.0);
        assert!(y >= p0.1 && y <= p2.1);

        // Check alignment with rows.
        assert!(
            params.row_min_y.contains(&y),
            "the movable cell should be aligned to the standard-cell rows"
        );
    }
}

/// Rust wrapper around C function of Coloquinte.
pub(crate) fn coloquinte_place(params: &mut ColoquinteParameters) -> i32 {
    // Validate parameter values. Panic on failure.
    params.check();

    log::debug!("call unsafe place()");

    let return_value = unsafe {
        place_ispd(
            params.num_cells.try_into().expect("can't convert to i32"),
            params.num_nets.try_into().expect("can't convert to i32"),
            params.cell_widths.as_ptr(),
            params.cell_heights.as_ptr(),
            params.cell_fixed.as_ptr(),
            params.net_limits.as_ptr(),
            params.pin_cells.as_ptr(),
            params.pin_x_offsets.as_ptr(),
            params.pin_y_offsets.as_ptr(),
            params.cell_x.as_mut_ptr(),
            params.cell_y.as_mut_ptr(),
            params.cell_orientation.as_mut_ptr(),
            params.num_rows.try_into().expect("can't convert to i32"),
            params.row_min_x.as_ptr(),
            params.row_max_x.as_ptr(),
            params.row_min_y.as_ptr(),
            params.row_max_y.as_ptr(),
            params.effort,
        )
    };

    println!("done unsafe place()");

    return_value
}

#[link(name = "coloquinte", kind = "static")]
extern "C" {
    /// Binding to C function of Coloquinte.
    fn place_ispd(
        nb_cells: i32,
        nb_nets: i32,
        cell_widths: *const i32,
        cell_heights: *const i32,
        cell_fixed: *const bool,
        net_limits: *const i32,
        pin_cells: *const i32,
        pin_x_offsets: *const i32,
        pin_y_offsets: *const i32,
        cell_x: *mut i32,
        cell_y: *mut i32,
        cell_orientation: *mut i32,
        nb_rows: i32,
        row_min_x: *const i32,
        row_max_x: *const i32,
        row_min_y: *const i32,
        row_max_y: *const i32,
        effort: i32,
    ) -> i32;
}
